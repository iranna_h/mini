package com.hcl.pp.exception;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	@ExceptionHandler(ConstraintViolationException.class)
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		ResponseEntity<Object> responseEntity = null;
		String errorMessage = "";
		List<ObjectError> errors = ex.getAllErrors();
		for (ObjectError e : errors) {
			errorMessage = e.getDefaultMessage();

			CustomExceptionHandler customError = new CustomExceptionHandler(errorMessage, request.getDescription(true),
					LocalDateTime.now());
			responseEntity = new ResponseEntity<>(customError, HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	
}
