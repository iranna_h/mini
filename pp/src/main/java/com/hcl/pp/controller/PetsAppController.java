package com.hcl.pp.controller;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.PetValidator;
import com.hcl.pp.validator.UserValidator;

@RestController
public class PetsAppController {

	@Autowired
	PetService petService;

	@Autowired
	UserService userService;

	private static final Logger logger = LogManager.getLogger(PetsAppController.class);

	@PostMapping("/user/add")
	public ResponseEntity<User> addUser(@Valid @RequestBody UserValidator userValidator) throws PetPeersException {
		User user = null;
		ResponseEntity<User> responseEntity = null;
		try {
			user = userService.addUser(userValidator);
			if (user != null) {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<User>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@PostMapping("/user/login")
	public ResponseEntity<Set<Pet>> loginUser(@Valid @RequestBody LoginValidator loginValidator) throws PetPeersException {
		Set<Pet> pets = null;
		ResponseEntity<Set<Pet>> responseEntity = null;
		try {
			pets = userService.loginUser(loginValidator);
			if (pets != null) {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<Pet>>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}


	@PostMapping("/pets/buyPet/{userId}/{petId}")
	public ResponseEntity<Pet> buyPet(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId,
			@PathVariable("petId") @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws PetPeersException {
		Pet petsBought = null;
		ResponseEntity<Pet> responseEntity = null;
		try {
			petsBought = userService.buyPet(userId, petId);
			if (petsBought != null) {
				responseEntity = new ResponseEntity<Pet>(petsBought, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Pet>(petsBought, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Pet>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	@GetMapping("/pets/myPets/{userId}")
	public ResponseEntity<Set<Pet>> myPets(@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		Set<Pet> pets = null;
		ResponseEntity<Set<Pet>> responseEntity = null;
		try {
			pets = userService.getMyPets(userId);
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<Pet>>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@GetMapping("/user/userByUserName/{userName}")
	public ResponseEntity<User> getUserByUsername(@PathVariable String userName) throws PetPeersException {
		User user = null;
		ResponseEntity<User> responseEntity = null;
		try {
			user = userService.findByUserName(userName);
			if (user != null) {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<User>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@GetMapping("/user/userByUserId/{userId}")
	public ResponseEntity<User> userDetail(@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		User userDetail = null;
		ResponseEntity<User> responseEntity = null;
		try {
			userDetail = userService.getUserById(userId);
			if (userDetail != null) {
				responseEntity = new ResponseEntity<User>(userDetail, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<User>(userDetail, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<User>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@PostMapping("/user/update/{userId}")
	public ResponseEntity<User> alterUser(@Valid @PathVariable @Min(value = 1, message = "Minimum entry must be One") Long userId, @RequestBody UserValidator userValidator) throws PetPeersException {
		User user = null;
		ResponseEntity<User> responseEntity = null;
		try {
			user = userService.updateUser(userId,userValidator);
			if (user != null) {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<User>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@GetMapping("/users")
	public ResponseEntity<Set<User>> getAllUsers() throws PetPeersException {
		Set<User> users = null;
		ResponseEntity<Set<User>> responseEntity = null;
		try {
			users = userService.listUsers();
			if (users != null && users.size() > 0) {
				responseEntity = new ResponseEntity<Set<User>>(users, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<User>>(users, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<User>>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@DeleteMapping
	public ResponseEntity<Void> deleteUser(@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long userId) throws PetPeersException {
		User user = null;
		ResponseEntity<Void> responseEntity = null;
		try {
			user = userService.getUserById(userId);
			if (user != null) {
				userService.removeUser(userId);
				responseEntity = new ResponseEntity<Void>(HttpStatus.GONE);
			} else {
				responseEntity = new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	
	}
	
	@PostMapping("/pets/addPet")
	public ResponseEntity<Pet> addPet(@Valid @RequestBody PetValidator petRequest) throws PetPeersException {
		Pet pets = null;
		ResponseEntity<Pet> responseEntity = null;
		try {
			pets = petService.savePet(petRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Pet>(pets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Pet>(pets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Pet>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@GetMapping("/pets")
	public ResponseEntity<Set<Pet>> petHome() throws PetPeersException {
		Set<Pet> pets = null;
		ResponseEntity<Set<Pet>> responseEntity = null;
		try {
			pets = petService.getAllPets();
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<Pet>>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<Pet>>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@GetMapping("/pets/petDetail/{owner}")
	public ResponseEntity<Pet> petByOwnerId(
			@PathVariable("owner") @Min(value = 1, message = "Minimum entry must be One") Long owner)
			throws PetPeersException {
		Pet petByOwnerId = null;
		ResponseEntity<Pet> responseEntity = null;
		try {
			petByOwnerId = petService.getPetById(owner);
			if (petByOwnerId != null) {
				responseEntity = new ResponseEntity<Pet>(petByOwnerId, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Pet>(petByOwnerId, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Pet>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
	
	@GetMapping("/pets/petDetail/{petId}")
	public ResponseEntity<Pet> petDetail(
			@PathVariable("petId") @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws PetPeersException {
		Pet petDetail = null;
		ResponseEntity<Pet> responseEntity = null;
		try {
			petDetail = petService.getPetById(petId);
			if (petDetail != null) {
				responseEntity = new ResponseEntity<Pet>(petDetail, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Pet>(petDetail, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Pet>(HttpStatus.CONFLICT);
		}
		return responseEntity;
	}
				
	

}
