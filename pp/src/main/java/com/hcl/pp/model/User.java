package com.hcl.pp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

@Data

@Entity
@Table(name = "PET_USER")

public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8857157150571833114L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "USER_NAME", length = 55, nullable = false, unique = true)
	private String userName;

	@Column(name = "USER_PASSWD", length = 55, nullable = false)
	private String userPassword;

	@Transient
	private String confirmPassword;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
	
	private Set<Pet> pets;
}