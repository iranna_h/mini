package com.hcl.pp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
@Entity
@Table(name = "PETS")
@Data

public class Pet implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2795014096442173136L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "PET_NAME", length = 55, nullable = false)
	private String name;

	@Column(name = "PET_AGE")
	private Integer age;

	@Column(name = "PET_PLACE", length = 55)
	private String place;

	@ManyToOne
	@JoinColumn(name = "PET_OWNERID")

	private User owner;
}